# Debian Builder

- Customize variables in `scripts/${BOARD_NAME}`.
- Build the system using `scripts/build`
- Write the system using `scripts/write`
