#! /usr/bin/env bash

NEW_HOSTNAME=${HOSTNAME}
TARGET_DEVICE=/dev/cryptodisk/root
TARGET_PARTITION=${TARGET_DEVICE}
TARGET_HOME_PARTITION=/dev/cryptodisk/home

ARCH=amd64
COMPONENTS=main,contrib,non-free
BACKPORTS=${SUITE}-backports
PACKAGES+=(
    acpi-support
    blackbird-gtk-theme
    blueman
    console-setup
    cryptsetup-initramfs
    dbus-user-session
    dmz-cursor-theme
    efibootmgr
    eject
    elementary-xfce-icon-theme
    evince
    firefox-esr-l10n-fr
    firmware-iwlwifi
    firmware-misc-nonfree
    firmware-realtek
    fwupd-amd64-signed
    gnome-calculator
    gnupg2
    gvfs-backends
    intel-microcode
    keyboard-configuration
    laptop-detect
    light-locker
    lightdm
    lightdm-gtk-greeter-settings
    lvm2
    man-db
    mousepad
    ntfs-3g
    oathtool
    openssh-client
    pass
    pavucontrol
    pinentry-gnome3
    plymouth
    plymouth-themes
    policykit-1
    pulseaudio
    pulseaudio-module-bluetooth
    pwgen
    qt5-style-plugins
    qt5ct
    ristretto
    suckless-tools
    thunderbird-l10n-fr
    tumbler-plugins-extra
    unzip
    usb-modeswitch
    vlc
    vlc-l10n
    vlc-plugin-notify
    wireless-regdb
    wpasupplicant
    x11-xserver-utils
    xclip
    xfce4-datetime-plugin
    xfce4-notifyd
    xfce4-panel
    xfce4-places-plugin
    xfce4-power-manager-plugins
    xfce4-pulseaudio-plugin
    xfce4-screenshooter
    xfce4-session
    xfce4-settings
    xfce4-terminal
    xfce4-whiskermenu-plugin
    xfdesktop4
    xfonts-scalable
    xfwm4
    xserver-xorg-input-libinput
    xserver-xorg-video-fbdev
    xserver-xorg-video-intel
    xserver-xorg-video-vesa
    xz-utils
)
KERNEL_PACKAGES=(
    linux-image-amd64
    grub-efi-amd64
)

WIFI_SSID=secret-ssid
WIFI_PASSPHRASE=secret-passphrase
LUKS_NAME=cryptodisk
LUKS_UUID=secret-uuid

function prepare_storage {
    # Clean first blocks of storage
    sudo dd if=/dev/zero of=${TARGET_DEVICE} bs=1M count=4
    # Create the filesystem
    sudo mkfs.ext4 ${TARGET_PARTITION}
    sudo mkfs.ext4 ${TARGET_HOME_PARTITION}
}

function mount_virtual_filesystems {
    sudo mount -t devtmpfs none ${1:-$NEW_ROOTFS}/dev
    sudo mount -t devpts none ${1:-$NEW_ROOTFS}/dev/pts
    sudo mount -t proc none ${1:-$NEW_ROOTFS}/proc
    sudo mount -t sysfs none ${1:-$NEW_ROOTFS}/sys
    sudo mount --bind /run ${1:-$NEW_ROOTFS}/run
}

function customize_base_packages {
    cat << EOF | sudo chroot ${NEW_ROOTFS} debconf-set-selections -
keyboard-configuration  keyboard-configuration/layout   select  Français
keyboard-configuration  keyboard-configuration/layoutcode   string  fr
keyboard-configuration  keyboard-configuration/model    select  PC générique 105 touches (internat.)
keyboard-configuration  keyboard-configuration/variant  select  Français
keyboard-configuration  keyboard-configuration/xkb-keymap   select  fr
EOF
    sudo rm ${NEW_ROOTFS}/etc/default/keyboard
    sudo chroot ${NEW_ROOTFS} dpkg-reconfigure --frontend noninteractive keyboard-configuration

    # Write crypttab
    echo "${LUKS_NAME}	UUID=${LUKS_UUID}	none	luks" | sudo tee ${NEW_ROOTFS}/etc/crypttab

    # Enable users listing in LightDM
    sudo mkdir -p ${NEW_ROOTFS}/etc/lightdm/lightdm.conf.d
    cat << EOF | sudo tee ${NEW_ROOTFS}/etc/lightdm/lightdm.conf.d/10-users.conf
[SeatDefaults]
greeter-hide-users=false
EOF

    # Configure Plymouth theme
    sudo chroot ${NEW_ROOTFS} plymouth-set-default-theme spinner

    sudo mkdir -p ${NEW_ROOTFS}/etc/default/grub.d
    # Enable Plymouth on kernel boot
    cat << EOF | sudo tee ${NEW_ROOTFS}/etc/default/grub.d/splash.cfg
GRUB_CMDLINE_LINUX_DEFAULT="\${GRUB_CMDLINE_LINUX_DEFAULT} splash"
EOF
    # Configure grub as non-gui
    cat << EOF | sudo tee ${NEW_ROOTFS}/etc/default/grub.d/grub-mode.cfg
GRUB_TERMINAL=console
EOF

    # Change X default cursor theme
    sudo chroot ${NEW_ROOTFS} update-alternatives --set x-cursor-theme /usr/share/icons/DMZ-White/cursor.theme
}

function custom_network_configuration {
    # Configure WiFi connection
    wpa_passphrase ${WIFI_SSID} ${WIFI_PASSPHRASE} | sudo tee ${NEW_ROOTFS}/etc/wpa_supplicant/wpa_supplicant-wlp1s0.conf > /dev/null
    sudo chmod o-rwx ${NEW_ROOTFS}/etc/wpa_supplicant/wpa_supplicant-wlp1s0.conf
    # sudo chroot ${NEW_ROOTFS} systemctl enable wpa_supplicant@wlp1s0.service
}

function customize_admin_user {
    # Allow required groups to run GUI applications
    sudo chroot ${NEW_ROOTFS} adduser ${ADMIN_USERNAME} audio
    sudo chroot ${NEW_ROOTFS} adduser ${ADMIN_USERNAME} video

    mkdir -p ${NEW_ROOTFS}/home/${ADMIN_USERNAME}/.config/pulse
    cat << EOF | sudo tee ${NEW_ROOTFS}/home/${ADMIN_USERNAME}/.config/pulse/default.pa
#!/usr/bin/pulseaudio -nF
.include /etc/pulse/default.pa
load-module module-combine-sink
EOF

    # Generate grub configuration file
    sudo chroot ${NEW_ROOTFS} update-grub
}

function umount_virtual_filesystems {
    sudo umount -R ${1:-$NEW_ROOTFS}/* || true
}

function mount_target {
    # Mount the destination storage in a temporary location
    sudo mount ${TARGET_PARTITION} ${MOUNT_POINT}
    sudo mkdir ${MOUNT_POINT}/home
    sudo mount ${TARGET_HOME_PARTITION} ${MOUNT_POINT}/home
}

function regenerate_initramfs {
    sudo chroot ${MOUNT_POINT} update-initramfs -ukall
}
